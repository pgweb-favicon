BASE_IMAGE=	PostgreSQL_elephant.svg

## PostgreSQL_elephant.svg image by Daniel Lundin
## Copyright © 2019, Daniel Lundin
## 
## Permission to use, copy, modify, and distribute this software and its
## documentation for any purpose, without fee, and without a written
## agreement is hereby granted, provided that the above copyright notice
## and this paragraph and the following two paragraphs appear in all
## copies.
## 
## IN NO EVENT SHALL THE AUTHOR BE LIABLE TO ANY PARTY FOR DIRECT,
## INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
## PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
## EVEN IF THE AUTHOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
## THE AUTHOR SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS
## IS" BASIS, AND THE AUTHOR HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE,
## SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS

## Obtained via Wikimedia Commons
## https://commons.wikimedia.org/wiki/File:Postgresql_elephant.svg

## Makefile code in this file is copyright (c) 2019, Marcin Cieślak
## 
## Permission is hereby granted, free of charge, to any person obtaining
## a copy of this software and associated documentation files (the
## "Software"), to deal in the Software without restriction, including
## without limitation the rights to use, copy, modify, merge, publish,
## distribute, sublicense, and/or sell copies of the Software, and to
## permit persons to whom the Software is furnished to do so, subject to
## the following conditions:
## 
## The above copyright notice and this permission notice shall be included
## in all copies or substantial portions of the Software.
## 
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
## EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
## MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
## IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
## CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
## TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

check:: favicon.ico
	winicontoppm -allicons -writeands $> f

favicon.ico:	icon16.pnm icon32.pnm icon48.pnm \
		icon16.and.pbm icon32.and.pbm icon48.and.pbm
	ppmtowinicon -andpgms \
		icon16.pnm icon16.and.pbm \
		icon32.pnm icon32.and.pbm \
		icon48.pnm icon48.and.pbm > $@

icon16.pnm:	$(BASE_IMAGE)
	rsvg-convert -w 16 -h 16 $> | pngtopnm | pnmdepth 4 > $@

icon32.pnm:	$(BASE_IMAGE)
	rsvg-convert -w 32 -h 32 $> | pngtopnm | pnmdepth 4 > $@

icon48.pnm:	$(BASE_IMAGE)
	rsvg-convert -w 48 -h 48 $> | pngtopnm | pnmdepth 8 > $@

icon16.and.pbm:	$(BASE_IMAGE)
	rsvg-convert -w 16 -h 16 $> | pngtopnm -alpha> $@

icon32.and.pbm:	$(BASE_IMAGE)
	rsvg-convert -w 32 -h 32 $> | pngtopnm -alpha> $@

icon48.and.pbm:	$(BASE_IMAGE)
	rsvg-convert -w 48 -h 48 $> | pngtopnm -alpha> $@

clean::
	rm -f icon16.pnm icon32.pnm icon48.pnm favicon.ico
	rm -f icon16.and.pbm icon32.and.pbm icon48.and.pbm
	rm -f f_and_0.pbm f_and_1.pbm f_and_2.pbm f_xor_0.ppm f_xor_1.ppm f_xor_2.ppm
